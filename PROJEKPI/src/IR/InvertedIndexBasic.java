/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IR;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 *
 * @author ASUS
 */
public class InvertedIndexBasic {

    private LinkedListOrderedUnique<Term> termList;

    public InvertedIndexBasic() {
        termList = new LinkedListOrderedUnique<Term>();
    }

    public LinkedListOrderedUnique getTermList() {
        return termList;
    }

    public void add(String pTerm, String pDocument) {
        Term vTerm = termList.get(new Term(pTerm));

        if (vTerm == null) {
            Term oTerm = new Term(pTerm);
            oTerm.documentList = new LinkedListOrderedUnique<Document>();
            oTerm.getDocumentList().addSort(new Document(pDocument));
            termList.addSort(oTerm);
        } else {
            vTerm.getDocumentList().addSort(new Document(pDocument));
        }
    }

    private LinkedList cari(String pTerm) {
        Term vTerm = termList.get(new Term(pTerm));
        LinkedList p = new LinkedList();
        if (vTerm == null) {
            System.out.println("Dokumen tidak ditemukan");
        } else {
            ListIterator iter = vTerm.getDocumentList().listIterator();
            while (iter.hasNext()) {
                p.add(iter.next());
            }
        }
        return p;
    }

    private LinkedList gabung(LinkedList p1, LinkedList p2) {
        LinkedList answer = new LinkedList();
        ListIterator pos1 = p1.listIterator();
        ListIterator pos2 = p2.listIterator();
        String temp1 = "";
        String temp2 = "";
        while (pos1.hasNext() && pos2.hasNext()) {
            temp1 = pos1.next().toString();
            temp2 = pos2.next().toString();
            if (temp1.equalsIgnoreCase(temp2)) {
                answer.add(temp1);
            } else if (temp1.compareTo(temp2) < 0) {
                temp2 = pos2.previous().toString();
            } else {
                temp1 = pos1.previous().toString();
            }
        }
        return answer;
    }

    public void search(String key) {
        String kalimat = sortByfrekuensi(key);
        LinkedList hasil = new LinkedList();
        String[] temp = kalimat.split(" ");
        LinkedList term = new LinkedList();
        for (int i = 0; i < temp.length; i++) {
            term.add(temp[i]);
        }
        hasil = cari(term.getFirst().toString());
        term.removeFirst();
        while (term != null && hasil != null && term.size() != 0) {
            hasil = gabung(hasil, cari(term.getFirst().toString()));
            term.removeFirst();
        }
        ListIterator iter = hasil.listIterator();
        String dok = "";
        while (iter.hasNext()) {
            dok = dok + " " + iter.next();
        }
        if (dok.equals("")) {
            System.out.println("Keyword tersebut tidak ditemukan di dokumen manapun");
        } else {
            System.out.println("Keyword tersebut berada di dokumen : " + dok);
        }
//        System.out.println(kalimat);
    }

    private String sortByfrekuensi(String kalimat) {
        String[] kata = kalimat.split(" ");
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for (int i = 0; i < kata.length; i++) {
            String temp = kata[i];
            Integer jumlah = map.get(temp);
            if (map.containsKey(temp)) {
                map.put(temp, jumlah + 1);
            } else {
                map.put(temp, 1);
            }
        }
        ArrayList tes = new ArrayList<>(map.keySet());
        Collections.sort(tes, new Comparator<String>() {
            @Override
            public int compare(String x, String y) {
                return map.get(x) - map.get(y);
            }
        });
        String coba = "";
        for (int i = 0; i < tes.size(); i++) {
            coba = coba + " " + tes.get(i);
        }
        coba = coba.replaceAll("\\s+", " ").trim();
        return coba;
    }
}
