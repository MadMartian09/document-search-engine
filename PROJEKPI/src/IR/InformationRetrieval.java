/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IR;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class InformationRetrieval {

    private Stopword stop;
    private Stem stem;
    private InvertedIndexBasic cobaList;

    public InformationRetrieval() throws IOException {
        stop = new Stopword();
        stem = new Stem();
        cobaList = new InvertedIndexBasic();
        bacaDokumen();
        pencarian();

    }

    private void bacaDokumen() {
        String path = "..\\Koleksi";
        Scanner input;

        File name = new File(path);
        if (name.exists()) {
            if (name.isDirectory()) {
                String[] directory = name.list();
                // System.out.println("\n\nDirectory contents:\n");

                for (String directoryName : directory) {
                    //System.out.println("\n" + directoryName);
                    try {
                        input = new Scanner(new File(name.getAbsolutePath() + "\\" + directoryName));
                        try {
                            while (input.hasNext()) {
                                //System.out.println(input.nextLine());
                                String[] tokens = stop.hapusStopWord(input.nextLine().trim()).split(" ");

                                for (String token : tokens) {

                                    //System.out.println(directoryName+" "+stem.KataDasar(token));
                                    cobaList.add(stem.KataDasar(token), directoryName);
                                   // System.out.println(stem.KataDasar(token)+ " " + directoryName);
                                }
                                //System.out.println(stop.hapusStopWord(input.nextLine()));

                            }
                        } catch (NoSuchElementException elementException) {
                            System.err.println("File improperly formed");
                            input.close();
                            System.exit(1);
                        } catch (IllegalStateException stateException) {
                            System.err.println("Error reading form file.");
                            System.exit(1);
                        }

                    } catch (FileNotFoundException e) {
                        System.err.println("Error Opening file");
                        System.exit(1);
                    }
                    System.out.println("");
                }
            }
        } else {
            System.out.println(path + " Does not exist");
        }
    }

    public void pencarian() {
        System.out.println("=======SELAMAT DATANG DI SISTEM PENCARIAN DOKUMEN==========");
        System.out.print("Input keyword yang anda cari : ");
        Scanner key = new Scanner(System.in);
        String inputUser = key.nextLine();
        inputUser = stop.hapusStopWord(inputUser);
        String[] term = inputUser.split(" ");
        String hasil = "";
        for (int i = 0; i < term.length; i++) {
            hasil = hasil + " " + stem.KataDasar(term[i]);
        }
        hasil = hasil.replaceAll("\\s+", " ").trim();
        cobaList.search(hasil);
    }

}
