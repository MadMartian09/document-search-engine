/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IR;

import java.io.File;
import java.io.FileNotFoundException;

import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Stopword {

    LinkedList stop; 

    public Stopword() {
        stop  = new LinkedList();
        bacaKamus();
    }

    
    private String cek(String kalimat) {
        if (stop.contains(kalimat)) {
            kalimat = kalimat.replaceAll(kalimat, "");
        }
        return kalimat;
    }
    
    private String HapusAngkadanTandaBaca(String kalimat) {
        kalimat = kalimat.replaceAll("\\p{Punct}|\\d", " ");
        return kalimat;
    }
    
    private String toLowerCase (String kalimat) {
        kalimat = kalimat.toLowerCase();
        return kalimat;
    }
    
    public String hapusStopWord(String kalimat) {
        kalimat = HapusAngkadanTandaBaca(kalimat);
        kalimat = toLowerCase(kalimat);
        String[] temp = kalimat.split(" ");
        String hasil = "";
        for (int i = 0; i < temp.length; i++) {
            hasil = hasil +" "+ cek(temp[i]);
        }
        hasil = hasil.replaceAll("\\s+", " ").trim();
        return hasil;
    } 
    
    private void bacaKamus() {
        String fileName = "src/stopword_list_tala.txt";

        try {
            // membaca file
            File myFile = new File(fileName);
            Scanner fileReader = new Scanner(myFile);

            // cetak isi file
            while (fileReader.hasNextLine()) {
                String data = fileReader.nextLine();
                stop.add(data);
            }

        } catch (FileNotFoundException e) {
            System.out.println("Terjadi Kesalahan: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
