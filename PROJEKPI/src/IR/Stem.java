/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IR;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Stem {

    private static LinkedList kamus;
    private String kata;
    private String akarKata;
    private boolean isHapusSuffix;
    private String bersihkan = "";

    public Stem() throws FileNotFoundException, IOException {
        kamus = new LinkedList();
        bacaKamus();
    }

    private void setKata(String kata) {
        this.kata = kata;
        this.akarKata = kata;
        bersihkan = "";
    }

    private boolean cekKamus(String kata) {
        if (kamus.contains(kata)) {
            return true;
        } else {
            return false;
        }
    }

    
    public String KataDasar(String kata) {
        setKata(kata);
        if (cekKamus(kata)) {
            return akarKata;
        } else {
            hapusInfleksionalSuffiks();
            hapusDerivationSuffiks();

        }
        return akarKata;
    }

    
    private void hapusInfleksionalSuffiks() {
        if (kata.endsWith("lah") || kata.endsWith("kah") || kata.endsWith("nya")
                || kata.endsWith("tah") || kata.endsWith("pun")) {
            kata = kata.substring(0, kata.length() - 3);
        } else if (kata.endsWith("ku") || kata.endsWith("mu")) {
            kata = kata.substring(0, kata.length() - 2);
        }
    }

   
    private void hapusDerivationSuffiks() {
        isHapusSuffix = false;
        bersihkan = kata;
        if (kata.endsWith("i")) {
            bersihkan = kata.substring(0, kata.length() - 1);
            isHapusSuffix = true;
        } else if (kata.endsWith("kan")) {
            bersihkan = kata.substring(0, kata.length() - 3);
            isHapusSuffix = true;
        } else if (kata.endsWith("an")) {
            bersihkan = kata.substring(0, kata.length() - 2);
            isHapusSuffix = true;
        }

       
        if (cekKamus(bersihkan)) {
            akarKata = bersihkan;
        } else {
            
            akarKata = bersihkan;
            if (isHapusSuffix == true) {
                derivationPrefiksA();
            } else {
                derivationPrefiksB();
            }

        }
    }

    private void derivationPrefiksA() {
        
        boolean no1 = (akarKata.startsWith("be") && akarKata.endsWith("i"));
        boolean no2 = akarKata.startsWith("di") && akarKata.endsWith("an");
        boolean no3 = akarKata.startsWith("ke") && (akarKata.endsWith("i") || akarKata.endsWith("kan"));
        boolean no4 = akarKata.startsWith("me") && akarKata.endsWith("an");
        boolean no5 = akarKata.startsWith("se") && (akarKata.endsWith("i") || akarKata.endsWith("kan"));
        
        if (((no1) || (no2) || (no4) || (no4) || (no5)) == false) {
            derivationPrefiksB();
        }
    }

    private void derivationPrefiksB() {
        
        if ((akarKata.startsWith("di") || akarKata.startsWith("ke") || akarKata.startsWith("se"))
                && akarKata.length() > 2) {
            akarKata = akarKata.substring(2, akarKata.length());
        } 
        else if ((akarKata.startsWith("te") || akarKata.startsWith("me")
                || akarKata.startsWith("be") || akarKata.startsWith("pe"))) {
            
            String kata2 = akarKata.substring(2, akarKata.length());
           
            if (kata2.length() > 3) {
                if ((kata2.charAt(0) == 'r') && (kata2.charAt(1) == 'r')) {
                    akarKata = kata2;
                } 
                else if ((kata2.charAt(0) == 'r') && (vowel(kata2.charAt(1)))) {
                    akarKata = kata2.substring(1, kata2.length());   
                } 
                else if ((kata2.charAt(0) == 'r') && !((kata2.charAt(1) == 'r') || (vowel(kata2.charAt(1))))
                        && (kata2.charAt(2) == 'e') && (kata2.charAt(3) == 'r') && (vowel(kata2.charAt(4)))) {
                    akarKata = kata2.substring(1, kata2.length());
                }
                else if ((kata2.charAt(0) == 'r') && !((kata2.charAt(1) == 'r') || (vowel(kata2.charAt(1))))
                        && (kata2.charAt(2) == 'e') && (kata2.charAt(3) == 'r') && !(vowel(kata2.charAt(4)))) {
                    akarKata = kata2;
                }
                else if ((kata2.charAt(0) == 'r') && !((kata2.charAt(1) == 'r') || (vowel(kata2.charAt(1))))
                        && !(kata2.charAt(2) == 'e' && kata2.charAt(3) == 'r')) {
                    akarKata = kata2.substring(1, kata2.length());
                }
                else if (!((kata2.charAt(0) == 'r') || (vowel(kata2.charAt(0))))
                        && (kata2.charAt(1) == 'e') && (kata2.charAt(2) == 'r') && (vowel(kata2.charAt(3)))) {
                    akarKata = kata2;
                }
                else if (!((kata2.charAt(0) == 'r') || (vowel(kata2.charAt(0))))
                        && (kata2.charAt(1) == 'e') && (kata2.charAt(2) == 'r') && (!vowel(kata2.charAt(3)))) {
                    akarKata = kata2;
                } else {
                    akarKata = kata2;
                }
            }
        }
    }

    private boolean vowel(char huruf) {
        if (huruf == 'a' || huruf == 'i' || huruf == 'u' || huruf == 'e' || huruf == 'o') {
            return true;
        }
        return false;
    }

    private static void bacaKamus() {
        String fileName = "src/kata-dasar-indonesia.txt";

        try {
            // membaca file
            File myFile = new File(fileName);
            Scanner fileReader = new Scanner(myFile);

            // cetak isi file
            while (fileReader.hasNextLine()) {
                String data = fileReader.nextLine();
                kamus.add(data);
            }

        } catch (FileNotFoundException e) {
            System.out.println("Terjadi Kesalahan: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
